<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller
{
    public function index()
    {
    	try {
    		$data = [
				'menu' => MenuController::getMenu(),
				'nm_unit' => RefUnitController::unitById(session('idUnit'))->nm_unit,
                'nama' => session('name'),
			];

			return view('layouts.app', $data);

    	} catch (\Exception $e) {
    		return $e->getMessage();
    		//return view('errors.503');
    	}
    }

    public function token()
    {
        return csrf_token();
    }
}
