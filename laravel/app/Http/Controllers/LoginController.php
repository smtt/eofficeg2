<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Login;
use Illuminate\Support\Facades\Hash;
use Session;

class LoginController extends Controller
{
    public function index()
	{
		return view('login');
	}

	/**
	 * Otentikasi user 
	 */
	public function authenticate(Request $request)
	{
		$username = $request->input('username');
		$password = $request->input('password');
		
		try {

			if($username == 'superadmin'){

				if( $password == 'p4ssw0rd!@)!(' ) {

					session([
						'authenticated' => true,
						'nip' => '060000060',
						'name' => 'Admin',
						'realName' => 'Administrator',
						'idUnit' => '1121101505',
						'level' => '00',
						'eselon' => '00',
						'jeselon' => '0',
						'jnskel' => 'L',
					]);

					return response()->json(['error' => false, 'message' => 'success']);

				} else {
					return response()->json(['error' => true, 'message' => $password]);
				}
				
			} else {
				$cekStatusLogin = Login::cekLogin($username, $password);

				// cek eksistensi user
				if(count($cekStatusLogin) != 0){

					/*if(Hash::check($password, $cekStatusLogin[0]->uPass) == true){*/
					if(sha1(md5($password)) == $cekStatusLogin[0]->uPass){

						// cek status keaktifan user
						if($cekStatusLogin[0]->uActive == 'y') {

							// jik user eksis, password cocok, dan status user aktif maka buat session
							session([
								'authenticated' => true,
								'nip' => $cekStatusLogin[0]->uNip,
								'name' => $cekStatusLogin[0]->uName,
								'realName' => $cekStatusLogin[0]->uRealName,
								'idUnit' => $cekStatusLogin[0]->idUnit,
								'level' => $cekStatusLogin[0]->uLevel,
								'eselon' => $cekStatusLogin[0]->eselon,
								'jeselon' => substr($cekStatusLogin[0]->eselon, 0, 1),
								'jnskel' => $cekStatusLogin[0]->sex
							]);

							return response()->json(['error' => false, 'message' => 'Selamat Datang di e-Office G2!']);
							
						} else {
							return response()->json(['error' => true, 'message' => 'Username tidak aktif!']);
						}
						
					} else {
						return response()->json(['error' => true, 'message' => 'Password tidak sesuai!']);
					}
					
				} else {
					return response()->json(['error' => true, 'message' => 'User tidak ditemukan!']);
				}
			}
		} catch(\Exception $e) {
			//return response()->json(['error' => true, 'message' => $e->getMessage()]);
			return response()->json(['error' => true, 'code' => '500', 'message' => 'Internal Server Error']);
		}
	}

	/**
	 * Logout/keluar dari aplikasi 
	 */
	public function logout()
	{
		session([
			'authenticated'	=> false,
			'nip' 			=> null,
			'name' 			=> null,
			'realName'		=> null,
			'idunit' 		=> null,
			'level' 		=> null,
			'jnskel' 		=> null
		]);
		
		Session::flush();
		return redirect()->guest('/');
	}
}
