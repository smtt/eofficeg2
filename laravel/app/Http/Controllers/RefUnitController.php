<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class RefUnitController extends Controller
{
    public static function unitById($kd_unit)
	{
		$unit = DB::select("
			SELECT t.*
			FROM(
				SELECT 	n.jbtnId AS id_jab,
						n.jbtnId AS idjab,
						n.idUnit AS kdunit,
						n.idUnit AS kd_unit,
						j.jbtnNama AS nm_unit,
						j.jbtnNama AS nmunit,
						j.jbtn AS jabatan,
						LENGTH(n.idUnit) AS pkr,
						j.eselon
				FROM pbn_ref.ref_unit n
				INNER JOIN(SELECT 	jbtnId,
									jbtnNama,
									jbtn,
									eselon
							FROM pbn_ref.ref_jabatan) j ON n.jbtnId = j.jbtnId
				WHERE LENGTH(n.idUnit) <= 17
				  AND RIGHT(n.idUnit, 3) != '000'
			) t
			WHERE t.kd_unit = ?
		", [
			$kd_unit
		]);

		return $unit[0];
	}
}
