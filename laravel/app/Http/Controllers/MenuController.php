<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use Session;
use DB;

class MenuController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public static function getMenu()
	{
		$html = '';
		$angular = '
			var app = angular.module("spa", ["ui.router","chieffancypants.loadingBar"]);
			app.config(function($stateProvider, $urlRouterProvider){
			$urlRouterProvider.otherwise("/");
			$stateProvider';

		//query menu level 1
		$ones = Menu::where('active',1)
			->where('category', 1)
			->orderBy('sequence')
			->orderBy('id')
			->get();

		//menampilkan menu level 1
		foreach($ones as $one) {
			
			if($one->have_child == 0) {
				
				if($one->url == ''){
					$html .= '<li class="nav-item">
								<a ui-sref="/" class="nav-link"><i class="'.$one->icon.'"></i> '.$one->title.'</a>
							</li>';
					$angular .= '.state("/", {
									url: "/",
									templateUrl: "partials/'.$one->filename.'"
								})';
				}
				else{
					$html .= '<li class="nav-item">
								<a ui-sref="'.$one->link_url.'" class="nav-link"><i class="'.$one->icon.'"></i> '.$one->title.'</a>
							</li>';
					$angular .= '.state("'.$one->link_url.'", {
									url: "'.$one->link_url.'",
									templateUrl: "partials/'.$one->filename.'"
								})';
				}
			}
			// punya sub menu
			else {
				$html .= '<li class="nav-item">
		                    <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
		                    	<i class="'.$one->icon.'"></i> '.$one->title.'
		                    </a>
		                    <div class="dropdown-menu dropdown-menu-arrow">';

				//query menu level 2
				$twos = Menu::where('active',1)
					->where('category',2)
					->where('parent_id',$one->id)
					->orderBY('sequence')
					->get();

				//menampilkan menu level 2
				foreach($twos as $two) {
					
					$icon = $two->icon;

					$html .= '<a ui-sref="'.$two->link_url.'" class="dropdown-item ">'.$two->title.'</a>';
					$angular .= '.state("'.$two->link_url.'", {
									url: "/'.$two->link_url.'",
									templateUrl: "partials/'.$two->filename.'"
								})';
				}
				
				$html .= '</div>';
				$html .= '</li>';
			}
		}

		$angular .= '
			.state("profile", {
				url: "/profile",
				templateUrl: "partials/profile.html"
			})
		});';

		$data = [
			'html' => $html,
			'angular' => $angular,
		];

		return $data;
	}
}
