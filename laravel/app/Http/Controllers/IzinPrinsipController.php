<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;
use clsTinyButStrong;

class IzinPrinsipController extends Controller
{
    protected $table = 'd_izip';

    public function index()
    {
    	$query = DB::select('
			select  a.id,
                    a.unitUndang,
                    a.nmGiat,
                    date_format(a.tglAwal, "%d %b %Y") as tglAwal,
                    date_format(a.tglAkhir, "%d %b %Y") as tglAkhir,
                    a.kota,
                    b.nmStatus,
                    b.isStart,
                    b.isFinish,
                    c.nama
			from pbn_izip.'.$this->table.' a
            left join pbn_izip.r_alur_status b on(a.idAlurStatus=b.id)
            left join pbn_emp.dt_emp c on(a.nip=c.nip)
            where a.nipPos=?
            order by a.id desc
    	',[
            session('nip')
        ]);
    	$rows = collect($query);

    	$datatables = Datatables::of($rows)
    		->addIndexColumn()
            ->addColumn('action', function($row){
    			return view('action.izin-prinsip', [
    				'id'        =>$row->id,
                    'isStart'   =>$row->isStart,
                    'isFinish'  =>$row->isFinish
    			]);
    		})
    		->make(true);
    	return $datatables;
    }

    public function store(Request $request)
    {
    	try {
    		//unit yg mengajukan
    		$kdunit = session('idUnit');
    		$kd_es3 = substr($kdunit, -17, -2);
            $kd_es2 = substr($kdunit, -17, -4);
            $row_es3 = DB::connection('pbn_ref')->table('ref_unit_base')->where('idUnit',$kd_es3)->first();
            if($row_es3 == null){
                $row_es2 = DB::connection('pbn_ref')->table('ref_unit_base')->where('idUnit',$kd_es2)->first();
                if(substr($kdunit, 10, 1) == '0'){
                    $kd_alur_unit = '01';
                }
                else{
                    $kd_alur_unit = '02';
                }
            }
            else{
                $kd_alur_unit = '03';
            }

            //eselon awal yg mengajukan
            $eselon = session('jeselon');
            $es_asal = $eselon.substr($kd_alur_unit,1,1);
            if($eselon>4){
                $es_asal = '0'.substr($kd_alur_unit,1,1);
            }

            //status awal dokumen
            $status = DB::connection('pbn_izip')->table('r_alur_status')
            	->where('kdAlurUnit', 'like', '%+'.$kd_alur_unit.'+%')
            	->where('kdTujuan', 'like', '%+'.$request->input('tujuan').'+%')
            	->where('esAsal', 'like', '%+'.$es_asal.'+%')
            	->where('isStart', 1)
            	->first();

            $insert = DB::connection('pbn_izip')->table($this->table)->insert([
                'nip'       => session('nip'),
                'idUnit'    => session('idUnit'),
                'esAsal'    => $es_asal,
                'nipPos'    => session('nip'),
                'idUnitPos' => session('idUnit'),
                'esPos'     => $es_asal,
                'tglAwal'   => $request->input('tgl_acara_awal'),
                'tglAkhir'  => $request->input('tgl_acara_akhir'),
                'idAlurStatus'=> $status->id,
                'kdAlurUnit'  => $kd_alur_unit,
                'noSurat'   => htmlspecialchars($request->input('no_surat')),
                'tglSurat'  => $request->input('tgl_surat'),
                'hal'       => htmlspecialchars($request->input('perihal')),
                'unitUndang'=> htmlspecialchars($request->input('unit_undang')),
                'nmGiat'    => htmlspecialchars($request->input('nm_giat')),
                'tujuanGiat'=> htmlspecialchars($request->input('tujuan_giat')),
                'kdTujuan'  => $request->input('tujuan'),
                'negara'    => htmlspecialchars($request->input('negara')),
                'kota'      => htmlspecialchars($request->input('kota')),
                'idBiaya'   => $request->input('biaya'),
                'ketBiaya'  => htmlspecialchars($request->input('biaya_lain')),
                'created_at'=> DB::raw('NOW()'),
            ]);

    		return $insert ? 'success' : 'Data gagal disimpan!';

    	} catch (\Exception $e) {
    		//return $e->getMessage();
    		return 'Exception: Hubungi Admin!';
    	}
    }

    public function edit($id)
    {
        $query = DB::connection('pbn_izip')->table($this->table)
            ->where('id', $id)
            ->first();

        return json_encode($query);
    }

    public function update(Request $request)
    {
        try {
            $update = DB::connection('pbn_izip')->table($this->table)
                ->where('id', $request->input('inp-id'))
                ->update([
                    'tglAwal'   => $request->input('tgl_acara_awal'),
                    'tglAkhir'  => $request->input('tgl_acara_akhir'),
                    'noSurat'   => htmlspecialchars($request->input('no_surat')),
                    'tglSurat'  => $request->input('tgl_surat'),
                    'hal'       => htmlspecialchars($request->input('perihal')),
                    'unitUndang'=> htmlspecialchars($request->input('unit_undang')),
                    'nmGiat'    => htmlspecialchars($request->input('nm_giat')),
                    'tujuanGiat'=> htmlspecialchars($request->input('tujuan_giat')),
                    'kdTujuan'  => $request->input('tujuan'),
                    'negara'    => htmlspecialchars($request->input('negara')),
                    'kota'      => htmlspecialchars($request->input('kota')),
                    'idBiaya'   => $request->input('biaya'),
                    'ketBiaya'  => htmlspecialchars($request->input('biaya_lain')),
                    'created_at'=> DB::raw('NOW()'),
                ]);

            return $update ? 'success' : 'Data gagal diubah!';

        } catch (\Exception $e) {
            //return $e->getMessage();
            return 'Exception: Hubungi Admin!';
        }
    }

    public function destroy(Request $request)
    {
        try {
            $delete = DB::connection('pbn_izip')->table($this->table)
                ->where('id', $request->input('id'))
                ->delete();

            return $delete ? 'success' : 'Data gagal dihapus!';

        } catch (\Exception $e) {
            //return $e->getMessage();
            return 'Exception: Hubungi Admin!';
        }
    }

    public function getReviewOption($id)
    {
        $izip = DB::connection('pbn_izip')->table($this->table)
            ->where('id', $id)
            ->first();
        $query_opsi = DB::connection('pbn_izip')->table('r_alur_status_map')
            ->where('idAlurStatus', $izip->idAlurStatus)
            ->where('kdAlurUnit', 'like', '%+'.$izip->kdAlurUnit.'+%')
            ->where('kdTujuan', 'like', '%+'.$izip->kdTujuan.'+%')
            ->orderBy('sequence')
            ->get();

        $html = '<option value="" style="display:none;">Pilih Status</option>';
        foreach ($query_opsi as $row) {
            $html .= '<option value="'.$row->idAlurStatusNext.'.'.$row->sequence.'">'.$row->opsi.'</option>';
        }

        echo $html;
    }

    public function postReview(Request $request)
    {
        try {
            
            $status = $request->input('opsi');
            $sts_exp= explode('.', $status);
            $sts_next = $sts_exp[0];
            $sequence = $sts_exp[1];

            $prev = DB::connection('pbn_izip')->table($this->table)
                ->where('id', $request->input('inp-id'))
                ->first();
            $alurNext = DB::connection('pbn_izip')->table('r_alur_status')
                ->where('id', $sts_next)
                ->first();

            //tolak
            if($sequence == 2){
                $nipPos     = $prev->nip;
                $idUnitPos  = $prev->idUnit;
                $esPos      = $prev->esAsal;
            }

            //setuju
            else{

                //alur belum finish                
                if($alurNext->isFinish != 1){

                    //posisi dari pelaksana
                    if(session('jeselon')>4){
                        
                        $next = DB::connection('pbn_kinerja')->table('dt_coach_emp')
                            ->where('nip', $prev->nipPos)
                            ->where('active', 'y')
                            ->first();
                        
                        $nipPos     = $next->coachNip;
                        $idUnitPos  = $next->unit;
                        $esPos      = '4'.substr($prev->kdAlurUnit,1,1);
                    }

                    //posisi dari pejabat
                    else{

                        //kepala seksi
                        if(session('jeselon')==4){
                            
                            $nextUnit = substr($prev->idUnitPos, -17, -2);
                            $nextEs = session('jeselon')-1;
                            $next = DB::connection('pbn_emp')->table('dt_emp')
                                ->where('unit', $nextUnit)
                                ->where(DB::raw('substr(eselon,1,1)'), '=', $nextEs)
                                ->where('active', 'y')
                                ->first();
                            
                            $nipPos     = $next->nip;
                            $idUnitPos  = $next->unit;
                            $esPos      = $nextEs.substr($prev->kdAlurUnit,1,1);
                        }

                        elseif(session('jeselon')==3){

                            if($prev->esPos == '33'){//dari kepala kppn

                                $nextUnit = substr($prev->idUnitPos, -17, -2).'91'; //ke kabu kanwil
                                $nextEs = session('jeselon');
                                $next = DB::connection('pbn_emp')->table('dt_emp')
                                    ->where('unit', $nextUnit)
                                    ->where(DB::raw('substr(eselon,1,1)'), '=', $nextEs)
                                    ->where('active', 'y')
                                    ->first();
                                
                                $nipPos     = $next->nip;
                                $idUnitPos  = $next->unit;
                                $esPos      = '32';
                            }

                            elseif($prev->esPos == '32'){//dari eselon 3 kanwil

                                if($prev->kdTujuan=='01'){ //dalam kota
                                    
                                    $nextUnit = substr($prev->idUnitPos, -17, -2); //ke kepala kanwil
                                    $nextEs = session('jeselon')-1;
                                    $next = DB::connection('pbn_emp')->table('dt_emp')
                                        ->where('unit', $nextUnit)
                                        ->where(DB::raw('substr(eselon,1,1)'), '=', $nextEs)
                                        ->where('active', 'y')
                                        ->first();
                                    
                                    $nipPos     = $next->nip;
                                    $idUnitPos  = $next->unit;
                                    $esPos      = '22';
                                }

                                else{

                                    if($prev->idAlurStatus == '52'){ //sebagai atasan langsung

                                        $nextUnit = substr($prev->idUnitPos, -17, -2).'91'; //ke kabu
                                        $nextEs = session('jeselon');
                                        $next = DB::connection('pbn_emp')->table('dt_emp')
                                            ->where('unit', $nextUnit)
                                            ->where(DB::raw('substr(eselon,1,1)'), '=', $nextEs)
                                            ->where('active', 'y')
                                            ->first();
                                        
                                        $esPos      = '32';
                                    }

                                    else{ //sebagai kabu

                                        $nextUnit = substr($prev->idUnitPos, -17, -2);
                                        $nextEs = session('jeselon')-1;
                                        $next = DB::connection('pbn_emp')->table('dt_emp')
                                            ->where('unit', $nextUnit)
                                            ->where(DB::raw('substr(eselon,1,1)'), '=', $nextEs)
                                            ->where('active', 'y')
                                            ->first();
                                        
                                        $esPos      = '22';
                                    }

                                    $nipPos     = $next->nip;
                                    $idUnitPos  = $next->unit;
                                }
                            }

                            elseif($prev->esPos == '31'){//dari eselon 3 kantor pusat

                                if($prev->idUnitPos == '112110150500101'){ //kabag OTL

                                    $nextUnit = '1121101505001'; //ke sesditjen
                                    $nextEs = session('jeselon')-1;
                                    $next = DB::connection('pbn_emp')->table('dt_emp')
                                        ->where('unit', $nextUnit)
                                        ->where(DB::raw('substr(eselon,1,1)'), '=', $nextEs)
                                        ->where('active', 'y')
                                        ->first();
                                }

                                else{ //kasubdit

                                    $nextUnit = substr($prev->idUnitPos, -17, -2);
                                    $nextEs = session('jeselon')-1;
                                    $next = DB::connection('pbn_emp')->table('dt_emp')
                                        ->where('unit', $nextUnit)
                                        ->where(DB::raw('substr(eselon,1,1)'), '=', $nextEs)
                                        ->where('active', 'y')
                                        ->first();
                                }

                                $nipPos     = $next->nip;
                                $idUnitPos  = $next->unit;
                                $esPos      = '21';
                            }
                        }

                        elseif(session('jeselon')==2){
                            
                            if($prev->esPos == '22'){//dari kepala kanwil

                                $nextUnit = '112110150500101'; //ke kabag OTL
                                $nextEs = session('jeselon')+1;
                                $next = DB::connection('pbn_emp')->table('dt_emp')
                                    ->where('unit', $nextUnit)
                                    ->where(DB::raw('substr(eselon,1,1)'), '=', $nextEs)
                                    ->where('active', 'y')
                                    ->first();
                                
                                $nipPos     = $next->nip;
                                $idUnitPos  = $next->unit;
                                $esPos      = '31';
                            }

                            elseif($prev->esPos == '21'){ //dari eselon 2 kantor pusat

                                if($prev->idUnitPos == '1121101505001'){ //sesditjen

                                    $nextUnit = '1121101505'; //ke DIRJEN
                                    $nextEs = session('jeselon')-1;
                                    $next = DB::connection('pbn_emp')->table('dt_emp')
                                        ->where('unit', $nextUnit)
                                        ->where(DB::raw('substr(eselon,1,1)'), '=', $nextEs)
                                        ->where('active', 'y')
                                        ->first();
                                    
                                    $esPos      = '11';
                                }

                                else{ //direktur

                                    $nextUnit = '112110150500101'; //ke kabag OTL
                                    $nextEs = session('jeselon')+1;
                                    $next = DB::connection('pbn_emp')->table('dt_emp')
                                        ->where('unit', $nextUnit)
                                        ->where(DB::raw('substr(eselon,1,1)'), '=', $nextEs)
                                        ->where('active', 'y')
                                        ->first();
                                    
                                    $esPos      = '31';
                                }

                                $nipPos     = $next->nip;
                                $idUnitPos  = $next->unit;
                            }
                        }
                    }
                }

                //alur sudah finish
                elseif($alurNext->isFinish == 1){
                    $nipPos     = $prev->nip;
                    $idUnitPos  = $prev->idUnit;
                    $esPos      = $prev->esAsal;
                }
            }


            DB::beginTransaction();

            $insert_new = DB::connection('pbn_izip')->table($this->table)->insert([
                'nip'           => $prev->nip,
                'idUnit'        => $prev->idUnit,
                'esAsal'        => $prev->esAsal,
                'nipPos'        => $nipPos,
                'idUnitPos'     => $idUnitPos,
                'esPos'         => $esPos,
                'tglAwal'       => $request->input('tgl_acara_awal'),
                'tglAkhir'      => $request->input('tgl_acara_akhir'),
                'idAlurStatus'  => $sts_next,
                'kdAlurUnit'    => $prev->kdAlurUnit,
                'noSurat'   => $prev->noSurat,
                'tglSurat'  => $prev->tglSurat,
                'hal'       => $prev->hal,
                'unitUndang'=> $prev->unitUndang,
                'nmGiat'    => $prev->nmGiat,
                'tujuanGiat'=> $prev->tujuanGiat,
                'kdTujuan'  => $prev->kdTujuan,
                'negara'    => $prev->negara,
                'kota'      => $prev->kota,
                'idBiaya'   => $prev->idBiaya,
                'ketBiaya'  => $prev->ketBiaya,
                'created_at'=> DB::raw('NOW()'),
            ]);

            if($insert_new){
                
                $ins_history = DB::connection('pbn_izip')->table('d_izip_history')->insert([
                    'id_izip'       => $prev->id,
                    'nip'           => $prev->nip,
                    'idUnit'        => $prev->idUnit,
                    'esAsal'        => $prev->esAsal,
                    'nipPos'        => $prev->nipPos,
                    'idUnitPos'     => $prev->idUnitPos,
                    'esPos'         => $prev->esPos,
                    'tglAwal'       => $prev->tglAwal,
                    'tglAkhir'      => $prev->tglAkhir,
                    'idAlurStatus'  => $prev->idAlurStatus,
                    'kdAlurUnit'    => $prev->kdAlurUnit,
                    'noSurat'   => $prev->noSurat,
                    'tglSurat'  => $prev->tglSurat,
                    'hal'       => $prev->hal,
                    'unitUndang'=> $prev->unitUndang,
                    'nmGiat'    => $prev->nmGiat,
                    'tujuanGiat'=> $prev->tujuanGiat,
                    'kdTujuan'  => $prev->kdTujuan,
                    'negara'    => $prev->negara,
                    'kota'      => $prev->kota,
                    'idBiaya'   => $prev->idBiaya,
                    'ketBiaya'  => $prev->ketBiaya,
                    'created_at'=> DB::raw('NOW()'),
                ]);

                $del_prev = DB::connection('pbn_izip')->table($this->table)
                    ->where('id', $request->input('inp-id'))
                    ->delete();

                if($ins_history && $del_prev){
                    DB::commit();
                    return 'success';
                }
                else{
                    return 'Data histori gagal disimpan!';
                }
            }
            else{
                return 'Data review gagal disimpan!';
            }

        } catch (\Exception $e) {
            return $e->getMessage();
            //return 'Exception: Hubungi Admin!';
        }
    }

    public function monitoring()
    {
        $query = DB::select('
            select  a.id,
                    a.unitUndang,
                    a.nmGiat,
                    date_format(a.tglAwal, "%d %b %Y") as tglAwal,
                    date_format(a.tglAkhir, "%d %b %Y") as tglAkhir,
                    a.kota,
                    b.nmStatus,
                    b.isStart,
                    b.isFinish,
                    c.nama,
                    d.created_at
            from pbn_izip.d_izip a
            left join pbn_izip.r_alur_status b on(a.idAlurStatus=b.id)
            left join pbn_emp.dt_emp c on(a.nip=c.nip)
            left join pbn_izip.d_izip_history d on(a.id=d.id_izip)
            where a.nip=? or a.nipPos=? or d.nip=? or d.nipPos=?
            order by d.created_at desc
        ',[
            session('nip'), session('nip'), session('nip'), session('nip')
        ]);
        $rows = collect($query);

        $datatables = Datatables::of($rows)
            ->addIndexColumn()
            ->make(true);
        return $datatables;
    }

    public function cetak_nd($id)
    {
        $izip = DB::connection('pbn_izip')->table($this->table)
            ->where('id',$id)
            ->first();

        $kop = DB::connection('pbn_mail')->table('mail_header')
            ->where('unit', $izip->idUnit)
            ->where('active', 'y')
            ->first();
        $nomor = DB::connection('pbn_ref')->table('ref_unit')
            ->where('idUnit', $izip->idUnit)
            ->first();
        $tahun = date("Y");

        if($izip->kdAlurUnit == '03'){
            $_param = (object)array(
                'head1' => $kop->head_1,
                'head2' => $kop->head_2,
                'head3' => $kop->head_3,
                'head4' => $kop->head_4,
                'left1' => $kop->left_1,
                'left2' => $kop->left_2,
                'left3' => $kop->left_3,
                'left4' => $kop->left_4,
                'kode' => $nomor->kodeSurat,
                'tahun' => $tahun,
                'unitUndang' => $izip->unitUndang,
                'nmGiat' => $izip->nmGiat,
                'noSurat' => $izip->noSurat,
                'unitPemohon' => ucfirst($kop->head_4),
            );
        }
        else{
            $_param = (object)array(
                'head1' => $kop->head_1,
                'head2' => $kop->head_2,
                'head3' => $kop->head_3,
                'head4' => '',
                'left1' => $kop->left_1,
                'left2' => $kop->left_2,
                'left3' => $kop->left_3,
                'left4' => $kop->left_4,
                'kode' => $nomor->kodeSurat,
                'tahun' => $tahun,
                'unitUndang' => $izip->unitUndang,
                'nmGiat' => $izip->nmGiat,
                'noSurat' => $izip->noSurat,
                'unitPemohon' => ucfirst($kop->head_3),
            );
        }
        $param[] = $_param;

        $TBS = new clsTinyButStrong();
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);

        if(session('jeselon') > 4){
            $TBS->LoadTemplate('template/izin_prinsip_pelaksana.docx');
            $TBS->MergeBlock('p', $param);
            //download file
            $TBS->Show(OPENTBS_DOWNLOAD, 'nd_izinPrinsip_'.$izip->nip.'.docx');
        }
        else{
            $TBS->LoadTemplate('template/izin_prinsip_pejabat.docx');
            $TBS->MergeBlock('p', $param);
            //download file
            $TBS->Show(OPENTBS_DOWNLOAD, 'nd_izinPrinsip_'.$izip->nip.'.docx');
        }
    }
}
