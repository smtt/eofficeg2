<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Login extends Model
{
    protected $connection = 'pbn_user';
	protected $table = 'user_data';
	public $primaryKey = 'id';
	protected $hidden = [
        'password', 'remember_token',
    ];

    public static function cekLogin($username, $password)
	{
		$rows = DB::select("
			SELECT 	u.*,
					e.sex,
					e.eselon
			FROM pbn_user.user_data u
			LEFT JOIN pbn_emp.dt_emp e ON e.nip = u.uNip
			WHERE u.uNip = ?
		", [$username]);

		return $rows;
	}
}
