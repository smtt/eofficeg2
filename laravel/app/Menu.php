<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $connection = 'mysql';
    
    protected $table = 'pbn_izip.user_menus';
    
    public $primaryKey = 'id';
    
    public $incrimenting = true;
}
