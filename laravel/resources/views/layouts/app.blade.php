<!doctype html>
<html lang="en" dir="ltr" ng-app="spa">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en" />
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="{{ asset('template/tabler/favicon.ico') }}" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('template/tabler/favicon.ico') }}">
    <title>{{ config('app.name') }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    
    {{--<script src="{{ asset('template/tabler/assets/js/require.min.js') }}"></script>
    <script>
      requirejs.config({
        baseUrl: 'template/tabler'
      });
    </script>--}}

    <link href="{{ asset('template/tabler/assets/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/alertify/themes/alertify.core.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/alertify/themes/alertify.default.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/chosen/chosen.min.css') }}" rel="stylesheet">

    <style type="text/css">
      .table-center{text-align: center;}
    </style>
  </head>
  
  <body class="">
    <div class="page">
      <div class="flex-fill">
        <div class="header py-4">
          <div class="container">
            <div class="d-flex">
              <a class="header-brand" href="{{ url('/') }}">
                <img src="{{ asset('template/img/logo_edjpb_left.png') }}" class="header-brand-img" alt="tabler logo">
              </a>
              <div class="d-flex order-lg-2 ml-auto">
                <div class="dropdown d-none d-md-flex">
                  <a class="nav-link icon" data-toggle="dropdown">
                    <i class="fe fe-bell"></i>
                    <!-- <span class="nav-unread"></span> -->
                  </a>
                  <!-- <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                    <a href="#" class="dropdown-item d-flex">
                      <span class="avatar mr-3 align-self-center" style="background-image: url(demo/faces/male/41.jpg)"></span>
                      <div>
                        <strong>Nathan</strong> pushed new commit: Fix page load performance issue.
                        <div class="small text-muted">10 minutes ago</div>
                      </div>
                    </a>
                    <a href="#" class="dropdown-item d-flex">
                      <span class="avatar mr-3 align-self-center" style="background-image: url(demo/faces/female/1.jpg)"></span>
                      <div>
                        <strong>Alice</strong> started new task: Tabler UI design.
                        <div class="small text-muted">1 hour ago</div>
                      </div>
                    </a>
                    <a href="#" class="dropdown-item d-flex">
                      <span class="avatar mr-3 align-self-center" style="background-image: url(demo/faces/female/18.jpg)"></span>
                      <div>
                        <strong>Rose</strong> deployed new version of NodeJS REST Api V3
                        <div class="small text-muted">2 hours ago</div>
                      </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item text-center">Mark all as read</a>
                  </div> -->
                </div>
                <div class="dropdown">
                  <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                    <span class="avatar" style="background-image: url(template/tabler/demo/faces/male/18.jpg)"></span>
                    <span class="ml-2 d-none d-lg-block">
                      <span class="text-default">{{ $nama }}</span>
                      <small class="text-muted d-block mt-1">{{ $nm_unit }}</small>
                    </span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                    <a class="dropdown-item" href="#">
                      <i class="dropdown-icon fe fe-user"></i> Profil
                    </a>
                    <a class="dropdown-item" href="#">
                      <i class="dropdown-icon fe fe-settings"></i> Pengaturan
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="logout">
                      <i class="dropdown-icon fe fe-log-out"></i> Keluar
                    </a>
                  </div>
                </div>
              </div>
              <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
              </a>
            </div>
          </div>
        </div>
        <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
          <div class="container">
            <div class="row align-items-center">
              <!-- <div class="col-lg-3 ml-auto">
                <form class="input-icon my-3 my-lg-0">
                  <input type="search" class="form-control header-search" placeholder="Search&hellip;" tabindex="1">
                  <div class="input-icon-addon">
                    <i class="fe fe-search"></i>
                  </div>
                </form>
              </div> -->
              <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                  
                  {!! $menu['html'] !!}

                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="my-3 my-md-5">
          
          <div class="container" ui-view>
          </div>

        </div>
      </div>
      
      <footer class="footer">
        <div class="container">
          <div class="row align-items-center flex-row-reverse">
            <div class="col-auto ml-lg-auto">
              <div class="row align-items-center">
                <div class="col-auto">
                  <ul class="list-inline list-inline-dots mb-0">
                    <li class="list-inline-item"><a href="{{ url('/') }}">Documentation</a></li>
                    <li class="list-inline-item">
                      <a href="http://devhaipedia.kemenkeu.go.id" target="_blank">FAQ</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
              Copyright © 2019 <a href="{{ url('/') }}">{{ config('app.name') }}</a>. All rights reserved.
            </div>
          </div>
        </div>
      </footer>
    </div>

    {{--<script src="{{ asset('template/tabler/assets/js/app.js') }}"></script>--}}
    <script src="{{ asset('template/tabler/assets/js/vendors/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('template/tabler/assets/js/vendors/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('template/tabler/assets/js/vendors/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('template/tabler/assets/js/vendors/selectize.min.js') }}"></script>
    <script src="{{ asset('template/tabler/assets/js/vendors/jquery.tablesorter.min.js') }}"></script>
    <script src="{{ asset('template/tabler/assets/js/vendors/jquery-jvectormap-2.0.3.min.js') }}"></script>
    <script src="{{ asset('template/tabler/assets/js/vendors/jquery-jvectormap-de-merc.js') }}"></script>
    <script src="{{ asset('template/tabler/assets/js/vendors/jquery-jvectormap-world-mill.js') }}"></script>
    <script src="{{ asset('template/tabler/assets/js/vendors/circle-progress.min.js') }}"></script>
    <script src="{{ asset('plugins/alertify/lib/alertify.min.js') }}"></script>
    <script src="{{ asset('plugins/chosen/chosen.jquery.min.js') }}"></script>

    <!-- Datatables Plugin -->
    {{--<script src="{{ asset('template/tabler/assets/plugins/datatables/plugin.js') }}"></script>--}}
    <script src="{{ asset('template/tabler/assets/plugins/datatables/datatables.min.js') }}"></script>

    <!-- load angular -->
    <script src="{{ asset('template/angular/angular.min.js') }}"></script>
    <script src="{{ asset('template/angular/angular-ui-router.min.js') }}"></script>
    <script src="{{ asset('template/angular/ngStorage.js') }}"></script>
    <script src="{{ asset('template/angular/loading-bar.js') }}"></script>
    <script>{!! $menu['angular'] !!}</script>

  </body>
</html>