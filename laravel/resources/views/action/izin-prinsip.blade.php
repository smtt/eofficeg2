<center>
	<div class="item-action dropdown">
		<a class="icon" href="javascript:void(0)" data-toggle="dropdown">
			<i class="fe fe-more-vertical"></i>
		</a>
		<div class="dropdown-menu dropdown-menu-right">

		@if($isFinish == 1)

			<a class="dropdown-item cetak" href="javascript:void(0)" id="{{ $id }}">
				<i class="fe fe-file-text"></i> ND Izin Prinsip
			</a>

		@else

			<a class="dropdown-item review" href="javascript:void(0)" id="{{ $id }}">
				<i class="fe fe-arrow-right"></i> Review Pengajuan
			</a>
		
			@if($isStart == 1)
				<a class="dropdown-item ubah" href="javascript:void(0)" id="{{ $id }}">
					<i class="fe fe-edit"></i> Edit
				</a>
				<a class="dropdown-item hapus" href="javascript:void(0)" id="{{ $id }}">
					<i class="fe fe-trash"></i> Hapus
				</a>
			@endif

		@endif
			
		</div>
	</div>
</center>