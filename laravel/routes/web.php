<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'LoginController@index')->name('login');
Route::post('postlogin', 'LoginController@authenticate');
Route::get('logout', 'LoginController@logout');

/*Auth::routes();*/

Route::group(['middleware'=>'auth'], function(){

	Route::get('/', 'AppController@index');
	Route::get('home', 'HomeController@index')->name('home');

	Route::get('token', 'AppController@token');

	Route::get('izinprinsip', 'IzinPrinsipController@index');
	Route::post('izinprinsip', 'IzinPrinsipController@store');
	Route::get('izinprinsip/edit/{id}', 'IzinPrinsipController@edit');
	Route::put('izinprinsip', 'IzinPrinsipController@update');
	Route::delete('izinprinsip', 'IzinPrinsipController@destroy');

	Route::get('izinprinsip/review/{id}', 'IzinPrinsipController@getReviewOption');
	Route::post('izinprinsip/review', 'IzinPrinsipController@postReview');

	Route::get('izinprinsip/cetak/nd/{id}', 'IzinPrinsipController@cetak_nd');

	Route::get('izinprinsip/mon', 'IzinPrinsipController@monitoring');

});
