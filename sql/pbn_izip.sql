-- --------------------------------------------------------
-- Host:                         10.242.231.108
-- Server version:               10.3.16-MariaDB-log - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pbn_izip.d_izip
CREATE TABLE IF NOT EXISTS `d_izip` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nip` varchar(18) NOT NULL,
  `idUnit` varchar(24) NOT NULL,
  `esAsal` char(2) NOT NULL,
  `nipPos` varchar(18) NOT NULL,
  `idUnitPos` varchar(24) NOT NULL,
  `esPos` char(2) NOT NULL,
  `noSurat` varchar(50) NOT NULL,
  `tglSurat` date NOT NULL,
  `hal` varchar(255) NOT NULL,
  `unitUndang` varchar(255) NOT NULL,
  `nmGiat` varchar(255) NOT NULL,
  `tujuanGiat` varchar(255) NOT NULL,
  `tglAwal` date NOT NULL,
  `tglAkhir` date NOT NULL,
  `idAlurStatus` int(11) NOT NULL,
  `kdAlurUnit` char(2) NOT NULL,
  `kdTujuan` char(2) NOT NULL,
  `negara` varchar(50) DEFAULT NULL,
  `kota` varchar(50) NOT NULL DEFAULT '',
  `idBiaya` int(11) NOT NULL,
  `ketBiaya` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;

-- Dumping data for table pbn_izip.d_izip: 9 rows
DELETE FROM `d_izip`;
/*!40000 ALTER TABLE `d_izip` DISABLE KEYS */;
INSERT INTO `d_izip` (`id`, `nip`, `idUnit`, `esAsal`, `nipPos`, `idUnitPos`, `esPos`, `noSurat`, `tglSurat`, `hal`, `unitUndang`, `nmGiat`, `tujuanGiat`, `tglAwal`, `tglAkhir`, `idAlurStatus`, `kdAlurUnit`, `kdTujuan`, `negara`, `kota`, `idBiaya`, `ketBiaya`, `created_at`) VALUES
	(27, '197007091999031001', '11211015051199103', '02', '197007091999031001', '11211015051199103', '02', 'Test', '2019-05-13', 'Test', 'Test', 'Test', 'Test', '2019-05-20', '2019-05-20', 3, '02', '01', '', 'Jakarta Selatan', 1, '', '2019-05-20 10:38:48'),
	(32, '197007091999031001', '11211015051199103', '02', '197007091999031001', '11211015051199103', '02', 'Halo', '2019-05-13', 'Test', 'BSSN', 'Diklat Lemsaneg', 'Test', '2019-05-20', '2019-05-24', 11, '02', '01', '', 'Test', 1, '', '2019-05-20 12:00:36'),
	(36, '199104072013102001', '11211015051120204', '03', '199104072013102001', '11211015051120204', '03', '1234567890', '2019-05-13', 'Narasumber', 'BSSN', 'Diklat', 'Fungsional', '2019-05-21', '2019-05-24', 6, '03', '01', '', 'Jakarta Selatan', 1, '', '2019-05-21 09:32:02'),
	(39, '197606221996032001', '112110150511604', '03', '197606221996032001', '112110150511604', '03', 'ND-50/WPB.16/BG.0101/2019', '2019-04-09', 'Undangan Nara Sumber', 'Kanwil DJPb Provinsi Jawa Timur', 'Seminar Directive Coaching Skill for effective leadership culture', 'berhasil dalam Coaching', '2019-04-24', '2019-04-26', 1, '03', '02', '', 'Surabaya', 2, 'DIPA BDK', '2019-06-17 16:00:31'),
	(38, '197007091999031001', '11211015051199103', '02', '197406281994021001', '11211015051199103', '42', '1234', '2019-05-24', 'Narsum', 'Kemenag', 'Fgd bendahara', 'Jadi narsum', '2019-05-29', '2019-05-31', 2, '02', '02', '', 'Pelaihari', 4, 'Kemenag', '2019-05-26 13:24:40'),
	(40, '197606221996032001', '112110150511604', '03', '197606221996032001', '112110150511604', '03', 'Und-51/UB/2019', '2019-05-02', 'Pelatihan Bendahara Pembantu', 'Universitas Brawijaya', 'Pelatihan Bendahara dan BPP', 'Bendahara Ahli mengelola keuangan dan pelaporan', '2019-05-20', '2019-05-24', 1, '03', '01', '', 'Malang', 4, 'DIPA Universitas Brawijaya', '2019-06-17 16:01:08'),
	(67, '199208102014112001', '11211015051010203', '03', '199208102014112001', '11211015051010203', '03', '48961', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-11', '2019-07-12', 19, '03', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 13:47:12'),
	(50, '199208102014112001', '11211015051010203', '03', '199208102014112001', '11211015051010203', '03', '57772', '2019-07-01', 'Undangan Narsum', 'Universitas  Syiah Kuala', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 11, '03', '02', '', 'Banda Aceh', 2, '', '2019-07-01 12:45:19'),
	(47, '199208102014112001', '11211015051010203', '03', '199208102014112001', '11211015051010203', '03', '3867564', '2019-07-01', 'Narsum', 'PNL', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-10', 6, '03', '01', '', 'Lhokseumawe', 3, '', '2019-07-01 10:56:17'),
	(58, '199208102014112001', '11211015051010203', '03', '199208102014112001', '11211015051010203', '03', '82575', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 16, '03', '03', '', 'Jakarta', 1, '', '2019-07-01 13:25:27'),
	(101, '199004012013101001', '11211015051019403', '02', '199004012013101001', '11211015051019403', '02', '4324567', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 19, '02', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 16:06:59'),
	(76, '199004012013101001', '11211015051019403', '02', '199004012013101001', '11211015051019403', '02', '437843', '2019-07-01', 'Undangan Narsum', 'Universitas  Syiah Kuala', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 11, '02', '01', '', 'Banda Aceh', 2, '', '2019-07-01 15:07:55'),
	(84, '199004012013101001', '11211015051019403', '02', '199004012013101001', '11211015051019403', '02', '563824', '2019-07-01', 'Undangan Narsum', 'KPPN Lhokseumawe', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 11, '02', '02', '', 'Lhokseumawe', 2, '', '2019-07-01 15:47:55'),
	(92, '199004012013101001', '11211015051019403', '02', '199004012013101001', '11211015051019403', '02', '2-80759', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-10', 16, '02', '03', '', 'Jakarta', 1, '', '2019-07-01 15:54:01'),
	(106, '199003272014111001', '11211015050080103', '01', '197005121990121001', '112110150500801', '31', '549262', '2019-07-01', 'Undangan Narsum', 'KPPN Jakarta II', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 52, '01', '01', '', 'Jakarta Pusat', 1, '', '2019-07-01 16:37:03');
/*!40000 ALTER TABLE `d_izip` ENABLE KEYS */;

-- Dumping structure for table pbn_izip.d_izip_history
CREATE TABLE IF NOT EXISTS `d_izip_history` (
  `id_izip` int(11) NOT NULL,
  `nip` varchar(18) NOT NULL,
  `idUnit` varchar(24) NOT NULL,
  `esAsal` char(2) NOT NULL DEFAULT '',
  `nipPos` varchar(18) NOT NULL,
  `idUnitPos` varchar(24) NOT NULL,
  `esPos` char(2) NOT NULL DEFAULT '',
  `noSurat` varchar(50) NOT NULL,
  `tglSurat` date NOT NULL,
  `hal` varchar(255) NOT NULL,
  `unitUndang` varchar(255) NOT NULL,
  `nmGiat` varchar(255) NOT NULL,
  `tujuanGiat` varchar(255) NOT NULL,
  `tglAwal` date NOT NULL,
  `tglAkhir` date NOT NULL,
  `idAlurStatus` int(11) NOT NULL,
  `kdAlurUnit` char(2) NOT NULL,
  `kdTujuan` char(2) NOT NULL,
  `negara` varchar(50) DEFAULT NULL,
  `kota` varchar(50) NOT NULL DEFAULT '',
  `idBiaya` int(11) NOT NULL,
  `ketBiaya` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table pbn_izip.d_izip_history: 18 rows
DELETE FROM `d_izip_history`;
/*!40000 ALTER TABLE `d_izip_history` DISABLE KEYS */;
INSERT INTO `d_izip_history` (`id_izip`, `nip`, `idUnit`, `esAsal`, `nipPos`, `idUnitPos`, `esPos`, `noSurat`, `tglSurat`, `hal`, `unitUndang`, `nmGiat`, `tujuanGiat`, `tglAwal`, `tglAkhir`, `idAlurStatus`, `kdAlurUnit`, `kdTujuan`, `negara`, `kota`, `idBiaya`, `ketBiaya`, `created_at`) VALUES
	(28, '197007091999031001', '11211015051199103', '02', '197007091999031001', '11211015051199103', '02', 'Halo', '2019-05-13', 'Test', 'BSSN', 'Diklat Lemsaneg', 'Test', '2019-05-20', '2019-05-24', 1, '02', '01', '', 'Test', 1, '', '2019-05-20 10:58:44'),
	(25, '197007091999031001', '11211015051199103', '02', '197007091999031001', '11211015051199103', '02', 'Test', '2019-05-13', 'Test', 'Test', 'Test', 'Test', '2019-05-20', '2019-05-20', 1, '02', '01', '', 'Jakarta Selatan', 1, '', '2019-05-20 10:35:50'),
	(26, '197007091999031001', '11211015051199103', '02', '197406281994021001', '11211015051199103', '42', 'Test', '2019-05-13', 'Test', 'Test', 'Test', 'Test', '2019-05-20', '2019-05-20', 2, '02', '01', '', 'Jakarta Selatan', 1, '', '2019-05-20 10:38:48'),
	(29, '197007091999031001', '11211015051199103', '02', '197406281994021001', '11211015051199103', '42', 'Halo', '2019-05-13', 'Test', 'BSSN', 'Diklat Lemsaneg', 'Test', '2019-05-20', '2019-05-24', 2, '02', '01', '', 'Test', 1, '', '2019-05-20 11:24:04'),
	(30, '197007091999031001', '11211015051199103', '02', '196312101985021001', '112110150511991', '32', 'Halo', '2019-05-13', 'Test', 'BSSN', 'Diklat Lemsaneg', 'Test', '2019-05-20', '2019-05-24', 52, '02', '01', '', 'Test', 1, '', '2019-05-20 11:40:38'),
	(31, '197007091999031001', '11211015051199103', '02', '196010141980101001', '1121101505119', '22', 'Halo', '2019-05-13', 'Test', 'BSSN', 'Diklat Lemsaneg', 'Test', '2019-05-20', '2019-05-24', 9, '02', '01', '', 'Test', 1, '', '2019-05-20 12:00:36'),
	(33, '199104072013102001', '11211015051120204', '03', '199104072013102001', '11211015051120204', '03', 'Test', '2019-05-13', 'Test', 'Test', 'Test', 'Test', '2019-05-20', '2019-05-24', 1, '03', '01', '', 'Jakarta Selatan', 1, '', '2019-05-21 09:24:38'),
	(34, '199104072013102001', '11211015051120204', '03', '197010191990121001', '11211015051120204', '43', 'Test', '2019-05-13', 'Test', 'Test', 'Test', 'Test', '2019-05-20', '2019-05-24', 2, '03', '01', '', 'Jakarta Selatan', 1, '', '2019-05-21 09:28:57'),
	(35, '199104072013102001', '11211015051120204', '03', '197006101997031001', '112110150511202', '33', 'Test', '2019-05-13', 'Test', 'Test', 'Test', 'Test', '2019-05-21', '2019-05-24', 4, '03', '01', '', 'Jakarta Selatan', 1, '', '2019-05-21 09:32:02'),
	(37, '197007091999031001', '11211015051199103', '02', '197007091999031001', '11211015051199103', '02', '1234', '2019-05-24', 'Narsum', 'Kemenag', 'Fgd bendahara', 'Jadi narsum', '2019-05-29', '2019-05-31', 1, '02', '02', '', 'Pelaihari', 4, 'Kemenag', '2019-05-26 13:24:40'),
	(41, '199208102014112001', '11211015051010203', '03', '199208102014112001', '11211015051010203', '03', '3867564', '2019-07-01', 'Narsum', 'PNL', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 1, '03', '01', '', 'Lhokseumawe', 3, '', '2019-07-01 09:44:02'),
	(43, '199208102014112001', '11211015051010203', '03', '199208102014112001', '11211015051010203', '03', '57772', '2019-07-01', 'Undangan Narsum', 'Universitas  Syiah Kuala', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 1, '03', '02', '', 'Banda Aceh', 2, '', '2019-07-01 10:41:06'),
	(42, '199208102014112001', '11211015051010203', '03', '196411091985032001', '11211015051010203', '43', '3867564', '2019-07-01', 'Narsum', 'PNL', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 2, '03', '01', '', 'Lhokseumawe', 3, '', '2019-07-01 10:42:31'),
	(44, '199208102014112001', '11211015051010203', '03', '196411091985032001', '11211015051010203', '43', '57772', '2019-07-01', 'Undangan Narsum', 'Universitas  Syiah Kuala', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 2, '03', '02', '', 'Banda Aceh', 2, '', '2019-07-01 10:51:51'),
	(45, '199208102014112001', '11211015051010203', '03', '196207151983031002', '112110150510102', '33', '3867564', '2019-07-01', 'Narsum', 'PNL', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 4, '03', '01', '', 'Lhokseumawe', 3, '', '2019-07-01 10:56:17'),
	(46, '199208102014112001', '11211015051010203', '03', '196207151983031002', '112110150510102', '33', '57772', '2019-07-01', 'Undangan Narsum', 'Universitas  Syiah Kuala', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 4, '03', '02', '', 'Banda Aceh', 2, '', '2019-07-01 12:11:14'),
	(48, '199208102014112001', '11211015051010203', '03', '197005201997031001', '112110150510191', '32', '57772', '2019-07-01', 'Undangan Narsum', 'Universitas  Syiah Kuala', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 7, '03', '02', '', 'Banda Aceh', 2, '', '2019-07-01 12:42:42'),
	(49, '199208102014112001', '11211015051010203', '03', '197203011997031001', '1121101505101', '22', '57772', '2019-07-01', 'Undangan Narsum', 'Universitas  Syiah Kuala', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 9, '03', '02', '', 'Banda Aceh', 2, '', '2019-07-01 12:45:19'),
	(51, '199208102014112001', '11211015051010203', '03', '199208102014112001', '11211015051010203', '03', '82575', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 1, '03', '03', '', 'Jakarta', 1, '', '2019-07-01 12:53:27'),
	(52, '199208102014112001', '11211015051010203', '03', '196411091985032001', '11211015051010203', '43', '82575', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 2, '03', '03', '', 'Jakarta', 1, '', '2019-07-01 12:54:14'),
	(53, '199208102014112001', '11211015051010203', '03', '196207151983031002', '112110150510102', '33', '82575', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 4, '03', '03', '', 'Jakarta', 1, '', '2019-07-01 12:55:02'),
	(54, '199208102014112001', '11211015051010203', '03', '197005201997031001', '112110150510191', '32', '82575', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 7, '03', '03', '', 'Jakarta', 1, '', '2019-07-01 12:56:00'),
	(55, '199208102014112001', '11211015051010203', '03', '197203011997031001', '1121101505101', '22', '82575', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 9, '03', '03', '', 'Jakarta', 1, '', '2019-07-01 13:15:27'),
	(56, '199208102014112001', '11211015051010203', '03', '197403271994021003', '112110150500101', '31', '82575', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 12, '03', '03', '', 'Jakarta', 1, '', '2019-07-01 13:24:36'),
	(57, '199208102014112001', '11211015051010203', '03', '196202161984012001', '1121101505001', '21', '82575', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 14, '03', '03', '', 'Jakarta', 1, '', '2019-07-01 13:25:27'),
	(59, '199208102014112001', '11211015051010203', '03', '199208102014112001', '11211015051010203', '03', '48961', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-08', 1, '03', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 13:31:42'),
	(60, '199208102014112001', '11211015051010203', '03', '196411091985032001', '11211015051010203', '43', '48961', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-08', 2, '03', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 13:32:42'),
	(61, '199208102014112001', '11211015051010203', '03', '196207151983031002', '112110150510102', '33', '48961', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-10', '2019-07-12', 4, '03', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 13:33:27'),
	(62, '199208102014112001', '11211015051010203', '03', '197005201997031001', '112110150510191', '32', '48961', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-10', '2019-07-12', 7, '03', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 13:34:25'),
	(63, '199208102014112001', '11211015051010203', '03', '197203011997031001', '1121101505101', '22', '48961', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-10', '2019-07-12', 9, '03', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 13:35:09'),
	(64, '199208102014112001', '11211015051010203', '03', '197403271994021003', '112110150500101', '31', '48961', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-10', '2019-07-12', 12, '03', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 13:36:10'),
	(65, '199208102014112001', '11211015051010203', '03', '196202161984012001', '1121101505001', '21', '48961', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-11', '2019-07-12', 14, '03', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 13:43:16'),
	(66, '199208102014112001', '11211015051010203', '03', '195906061983121001', '1121101505', '11', '48961', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-11', '2019-07-12', 17, '03', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 13:47:12'),
	(74, '199004012013101001', '11211015051019403', '02', '196410201985031003', '112110150510194', '32', '437843', '2019-07-01', 'Undangan Narsum', 'Universitas  Syiah Kuala', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 52, '02', '01', '', 'Banda Aceh', 2, '', '2019-07-01 15:07:25'),
	(73, '199004012013101001', '11211015051019403', '02', '198202182004121001', '11211015051019403', '42', '437843', '2019-07-01', 'Undangan Narsum', 'Universitas  Syiah Kuala', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 2, '02', '01', '', 'Banda Aceh', 2, '', '2019-07-01 15:05:48'),
	(72, '199004012013101001', '11211015051019403', '02', '199004012013101001', '11211015051019403', '02', '437843', '2019-07-01', 'Undangan Narsum', 'Universitas  Syiah Kuala', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 1, '02', '01', '', 'Banda Aceh', 2, '', '2019-07-01 15:04:00'),
	(75, '199004012013101001', '11211015051019403', '02', '197203011997031001', '1121101505101', '22', '437843', '2019-07-01', 'Undangan Narsum', 'Universitas  Syiah Kuala', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 9, '02', '01', '', 'Banda Aceh', 2, '', '2019-07-01 15:07:55'),
	(77, '199004012013101001', '11211015051019403', '02', '199004012013101001', '11211015051019403', '02', '563824', '2019-07-01', 'Undangan Narsum', 'KPPN Lhokseumawe', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 1, '02', '02', '', 'Lhokseumawe', 2, '', '2019-07-01 15:09:31'),
	(78, '199004012013101001', '11211015051019403', '02', '198202182004121001', '11211015051019403', '42', '563824', '2019-07-01', 'Undangan Narsum', 'KPPN Lhokseumawe', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 2, '02', '02', '', 'Lhokseumawe', 2, '', '2019-07-01 15:09:59'),
	(81, '199004012013101001', '11211015051019403', '02', '196410201985031003', '112110150510194', '32', '563824', '2019-07-01', 'Undangan Narsum', 'KPPN Lhokseumawe', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 52, '02', '02', '', 'Lhokseumawe', 2, '', '2019-07-01 15:46:30'),
	(82, '199004012013101001', '11211015051019403', '02', '197005201997031001', '112110150510191', '32', '563824', '2019-07-01', 'Undangan Narsum', 'KPPN Lhokseumawe', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 7, '02', '02', '', 'Lhokseumawe', 2, '', '2019-07-01 15:47:19'),
	(83, '199004012013101001', '11211015051019403', '02', '197203011997031001', '1121101505101', '22', '563824', '2019-07-01', 'Undangan Narsum', 'KPPN Lhokseumawe', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 9, '02', '02', '', 'Lhokseumawe', 2, '', '2019-07-01 15:47:55'),
	(85, '199004012013101001', '11211015051019403', '02', '199004012013101001', '11211015051019403', '02', '2-80759', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-10', 1, '02', '03', '', 'Jakarta', 1, '', '2019-07-01 15:51:08'),
	(86, '199004012013101001', '11211015051019403', '02', '198202182004121001', '11211015051019403', '42', '2-80759', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-10', 2, '02', '03', '', 'Jakarta', 1, '', '2019-07-01 15:51:41'),
	(87, '199004012013101001', '11211015051019403', '02', '196410201985031003', '112110150510194', '32', '2-80759', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-10', 52, '02', '03', '', 'Jakarta', 1, '', '2019-07-01 15:52:25'),
	(88, '199004012013101001', '11211015051019403', '02', '197005201997031001', '112110150510191', '32', '2-80759', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-10', 7, '02', '03', '', 'Jakarta', 1, '', '2019-07-01 15:52:49'),
	(89, '199004012013101001', '11211015051019403', '02', '197203011997031001', '1121101505101', '22', '2-80759', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-10', 9, '02', '03', '', 'Jakarta', 1, '', '2019-07-01 15:53:12'),
	(90, '199004012013101001', '11211015051019403', '02', '197403271994021003', '112110150500101', '31', '2-80759', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-10', 12, '02', '03', '', 'Jakarta', 1, '', '2019-07-01 15:53:37'),
	(91, '199004012013101001', '11211015051019403', '02', '196202161984012001', '1121101505001', '21', '2-80759', '2019-07-01', 'Undangan Narsum', 'Kantor Pusat', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-10', 14, '02', '03', '', 'Jakarta', 1, '', '2019-07-01 15:54:01'),
	(93, '199004012013101001', '11211015051019403', '02', '199004012013101001', '11211015051019403', '02', '4324567', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 1, '02', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 15:56:02'),
	(94, '199004012013101001', '11211015051019403', '02', '198202182004121001', '11211015051019403', '42', '4324567', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 2, '02', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 15:56:25'),
	(95, '199004012013101001', '11211015051019403', '02', '196410201985031003', '112110150510194', '32', '4324567', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 52, '02', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 16:04:48'),
	(96, '199004012013101001', '11211015051019403', '02', '197005201997031001', '112110150510191', '32', '4324567', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 7, '02', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 16:05:10'),
	(97, '199004012013101001', '11211015051019403', '02', '197203011997031001', '1121101505101', '22', '4324567', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 9, '02', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 16:05:41'),
	(98, '199004012013101001', '11211015051019403', '02', '197403271994021003', '112110150500101', '31', '4324567', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 12, '02', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 16:06:05'),
	(99, '199004012013101001', '11211015051019403', '02', '196202161984012001', '1121101505001', '21', '4324567', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 14, '02', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 16:06:25'),
	(100, '199004012013101001', '11211015051019403', '02', '195906061983121001', '1121101505', '11', '4324567', '2019-07-01', 'Undangan Narsum', 'Oracle', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 17, '02', '04', 'Australia', 'Sidney', 4, 'Oracle', '2019-07-01 16:06:59'),
	(102, '199108082013101004', '11211015050080104', '01', '199108082013101004', '11211015050080104', '01', '9824894', '2019-07-01', 'Undangan Narsum', 'KPPN Jakarta II', 'Diklat', 'Pelatihan Akuntansi', '2019-07-09', '2019-07-12', 1, '01', '01', '', 'Jakarta Pusat', 1, '', '2019-07-01 16:18:27'),
	(104, '199003272014111001', '11211015050080103', '01', '199003272014111001', '11211015050080103', '01', '549262', '2019-07-01', 'Undangan Narsum', 'KPPN Jakarta II', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 1, '01', '01', '', 'Jakarta Pusat', 1, '', '2019-07-01 16:32:47'),
	(105, '199003272014111001', '11211015050080103', '01', '196405061985031004', '11211015050080103', '41', '549262', '2019-07-01', 'Undangan Narsum', 'KPPN Jakarta II', 'Diklat', 'Pelatihan Akuntansi', '2019-07-08', '2019-07-12', 2, '01', '01', '', 'Jakarta Pusat', 1, '', '2019-07-01 16:37:03');
/*!40000 ALTER TABLE `d_izip_history` ENABLE KEYS */;

-- Dumping structure for table pbn_izip.r_alur_status
CREATE TABLE IF NOT EXISTS `r_alur_status` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `kdAlurUnit` varchar(50) NOT NULL,
  `kdTujuan` varchar(50) NOT NULL,
  `esAsal` varchar(50) NOT NULL DEFAULT '',
  `esPos` varchar(50) NOT NULL,
  `nmStatus` varchar(50) NOT NULL,
  `idUnit` varchar(25) DEFAULT NULL,
  `isStart` tinyint(1) DEFAULT 0,
  `isFinish` tinyint(1) DEFAULT 0,
  `ket` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

-- Dumping data for table pbn_izip.r_alur_status: 65 rows
DELETE FROM `r_alur_status`;
/*!40000 ALTER TABLE `r_alur_status` DISABLE KEYS */;
INSERT INTO `r_alur_status` (`id`, `kdAlurUnit`, `kdTujuan`, `esAsal`, `esPos`, `nmStatus`, `idUnit`, `isStart`, `isFinish`, `ket`) VALUES
	(1, '+03+02+01+', '+01+02+03+04+', '+03+02+01+', '+03+02+01+', 'Rekam Data', '', 1, 0, NULL),
	(2, '+03+02+01+', '+01+02+03+04+', '+03+02+01+', '+43+42+41+', 'Verifikasi Kasi/Kasubbag', '', 0, 0, NULL),
	(3, '+03+02+01+', '+01+02+03+04+', '+03+02+01+', '+03+02+01+', 'Ditolak Kasi/Kasubbag', '', 0, 0, NULL),
	(4, '+03+', '+01+02+03+04+', '+03+', '+33+', 'Verifikasi Kepala KPPN', '', 0, 0, NULL),
	(5, '+03+', '+01+02+03+04+', '+03+', '+03+', 'Ditolak Kepala KPPN', '', 0, 0, NULL),
	(6, '+03+', '+01+', '+03+', '+03+', 'Disetujui Kepala KPPN', '', 0, 1, NULL),
	(7, '+03+02+', '+02+03+04+', '+03+02+', '+32+', 'Verifikasi KaBU Kanwil', '', 0, 0, NULL),
	(8, '+03+02+', '+02+03+04+', '+03+02+', '+03+02+', 'Ditolak KaBU Kanwil', '', 0, 0, NULL),
	(9, '+03+02+', '+01+02+03+04+', '+03+02+', '+22+', 'Verifikasi Kepala Kanwil', '', 0, 0, NULL),
	(10, '+03+02+', '+01+02+03+04+', '+03+02+', '+03+02+', 'Ditolak Kepala Kanwil', '', 0, 0, NULL),
	(11, '+03+02+', '+01+02+', '+03+02+', '+03+02+', 'Disetujui Kepala Kanwil', '', 0, 1, NULL),
	(12, '+03+02+', '+03+04+', '+03+02+', '+31+', 'Verifikasi KaBag OTL', '112110150500101', 0, 0, NULL),
	(13, '+03+02+', '+03+04+', '+03+02+', '+03+02+', 'Ditolak KaBag OTL', '', 0, 0, NULL),
	(14, '+03+02+', '+03+04+', '+03+02+', '+21+', 'Verifikasi Sesditjen', '1121101505000', 0, 0, NULL),
	(15, '+03+02+', '+03+04+', '+03+02+', '+03+02+', 'Ditolak Sesditjen', '', 0, 0, NULL),
	(16, '+03+02+', '+03+', '+03+02+', '+03+02+', 'Disetujui Sesditjen', '', 0, 1, NULL),
	(17, '+03+02+', '+04+', '+03+02+', '+11+', 'Verifikasi Dirjen', '1121101505', 0, 0, NULL),
	(18, '+03+02+', '+04+', '+03+02+', '+03+02+', 'Ditolak Dirjen', '', 0, 0, NULL),
	(19, '+03+02+', '+04+', '+03+02+', '+03+02+', 'Disetujui Dirjen', '', 0, 1, NULL),
	(20, '+03+02+', '+01+02+03+04+', '+43+42+', '+43+42+', 'Rekam Data', NULL, 1, 0, NULL),
	(22, '+03+', '+01+02+03+04+', '+43+', '+43+', 'Ditolak Kepala KPPN', NULL, 0, 0, NULL),
	(23, '+03+', '+01+', '+43+', '+43+', 'Disetujui Kepala KPPN', NULL, 0, 1, NULL),
	(25, '+03+02+', '+02+03+04+', '+43+42+', '+43+42+', 'Ditolak KaBU Kanwil', NULL, 0, 0, NULL),
	(27, '+03+02+', '+01+02+03+04+', '+43+42+', '+43+42+', 'Ditolak Kepala Kanwil', NULL, 0, 0, NULL),
	(28, '+03+02+', '+01+02+', '+43+42+', '+43+42+', 'Disetujui Kepala Kanwil', NULL, 0, 1, NULL),
	(30, '+03+02+', '+03+04+', '+43+42+', '+43+42+', 'Ditolak KaBag OTL', NULL, 0, 0, NULL),
	(32, '+03+02+', '+03+04+', '+43+42+', '+43+42+', 'Ditolak Sesditjen', NULL, 0, 0, NULL),
	(33, '+03+02+', '+03+', '+43+42+', '+43+42+', 'Disetujui Sesditjen', NULL, 0, 1, NULL),
	(35, '+03+02+', '+04+', '+43+42+', '+43+42+', 'Ditolak Dirjen', NULL, 0, 0, NULL),
	(36, '+03+02+', '+04+', '+43+42+', '+43+42+', 'Disetujui Dirjen', NULL, 0, 1, NULL),
	(37, '+03+02+', '+01+02+03+04+', '+33+32+', '+33+32+', 'Rekam Data', NULL, 1, 0, NULL),
	(40, '+03+02+', '+02+03+04+', '+33+32+', '+33+32+', 'Ditolak KaBU Kanwil', NULL, 0, 0, NULL),
	(42, '+03+02+', '+01+02+03+04+', '+33+32+', '+33+32+', 'Ditolak Kepala Kanwil', NULL, 0, 0, NULL),
	(43, '+03+02+', '+01+02+', '+33+32+', '+33+32+', 'Disetujui Kepala Kanwil', NULL, 0, 1, NULL),
	(45, '+03+02+', '+03+04+', '+33+32+', '+33+32+', 'Ditolak KaBag OTL', NULL, 0, 0, NULL),
	(47, '+03+02+', '+03+04+', '+33+32+', '+33+32+', 'Ditolak Sesditjen', NULL, 0, 0, NULL),
	(48, '+03+02+', '+03+', '+33+32+', '+33+32+', 'Disetujui Sesditjen', NULL, 0, 1, NULL),
	(38, '+03+', '+01+', '+33+', '+33+', 'Rekam Data & Setuju', NULL, 1, 1, NULL),
	(50, '+03+02+', '+04+', '+33+32+', '+33+32+', 'Ditolak Dirjen', NULL, 0, 0, NULL),
	(51, '+03+02+', '+04+', '+33+32+', '+33+32+', 'Disetujui Dirjen', NULL, 0, 1, NULL),
	(52, '+02+01+', '+01+02+03+04+', '+02+01+', '+32+31+', 'Verifikasi KaBid/KaBag/Kasubdit', NULL, 0, 0, NULL),
	(53, '+02+01+', '+01+02+03+04+', '+02+01+', '+02+01+', 'Ditolak KaBid/KaBag/Kasubdit', NULL, 0, 0, NULL),
	(21, '+03+', '+01+02+03+04+', '+43+', '+33+', 'Verifikasi Kepala KPPN', NULL, 0, 0, NULL),
	(24, '+03+02+', '+02+03+04+', '+43+42+', '+32+', 'Verifikasi KaBU Kanwil', NULL, 0, 0, NULL),
	(26, '+03+02+', '+01+02+03+04+', '+43+42+', '+22+', 'Verifikasi Kepala Kanwil', NULL, 0, 0, NULL),
	(39, '+03+02+', '+02+03+04+', '+33+32+', '+33+32+', 'Verifikasi KaBU Kanwil', NULL, 0, 0, NULL),
	(41, '+03+02+', '+01+02+03+04+', '+33+32+', '+22+', 'Verifikasi Kepala Kanwil', NULL, 0, 0, NULL),
	(29, '+03+02+', '+03+04+', '+43+42+', '+31+', 'Verifikasi KaBag OTL', '112110150500101', 0, 0, NULL),
	(31, '+03+02+', '+03+04+', '+43+42+', '+21+', 'Verifikasi Sesditjen', '1121101505000', 0, 0, NULL),
	(44, '+03+02+', '+03+04+', '+33+32+', '+31+', 'Verifikasi KaBag OTL', '112110150500101', 0, 0, NULL),
	(46, '+03+02+', '+03+04+', '+33+32+', '+21+', 'Verifikasi Sesditjen', '1121101505000', 0, 0, NULL),
	(49, '+03+02+', '+04+', '+33+32+', '+11+', 'Verifikasi Dirjen', '1121101505', 0, 0, NULL),
	(34, '+03+02+', '+04+', '+43+42+', '+11+', 'Verifikasi Dirjen', '1121101505', 0, 0, NULL),
	(55, '+02+', '+01+02+03+04+', '+42+', '+32+', 'Verifikasi KaBid/KaBag/Kasubdit', NULL, 0, 0, NULL),
	(56, '+02+', '+01+02+03+04+', '+42+', '+42+', 'Ditolak KaBid/KaBag/Kasubdit', NULL, 0, 0, NULL),
	(58, '+02+', '+01+02+', '+22+', '+22+', 'Rekam Data & Setuju', NULL, 1, 1, NULL),
	(57, '+02+', '+03+04+', '+22+', '+22+', 'Rekam Data', NULL, 1, 0, NULL),
	(59, '+02+', '+03+04+', '+22+', '+31+', 'Verifikasi KaBag OTL', NULL, 0, 0, NULL),
	(60, '+02+', '+03+04+', '+22+', '+22+', 'Ditolak KaBag OTL', NULL, 0, 0, NULL),
	(61, '+02+', '+03+04+', '+22+', '+21+', 'Verifikasi Sesditjen', NULL, 0, 0, NULL),
	(62, '+02+', '+03+04+', '+22+', '+22+', 'Ditolak Sesditjen', NULL, 0, 0, NULL),
	(63, '+02+', '+03+', '+22+', '+22+', 'Disetujui Sesditjen', NULL, 0, 1, NULL),
	(64, '+02+', '+04+', '+22+', '+11+', 'Verifikasi Dirjen', NULL, 0, 0, NULL),
	(65, '+02+', '+04+', '+22+', '+22+', 'Ditolak Dirjen', NULL, 0, 0, NULL),
	(66, '+02+', '+04+', '+22+', '+22+', 'Disetujui Dirjen', NULL, 0, 1, NULL);
/*!40000 ALTER TABLE `r_alur_status` ENABLE KEYS */;

-- Dumping structure for table pbn_izip.r_alur_status_map
CREATE TABLE IF NOT EXISTS `r_alur_status_map` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `idAlurStatus` smallint(4) NOT NULL,
  `kdAlurUnit` varchar(50) NOT NULL,
  `kdTujuan` varchar(50) DEFAULT NULL,
  `opsi` char(50) NOT NULL,
  `idAlurStatusNext` smallint(4) NOT NULL,
  `sequence` smallint(1) DEFAULT NULL,
  `ket` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;

-- Dumping data for table pbn_izip.r_alur_status_map: 64 rows
DELETE FROM `r_alur_status_map`;
/*!40000 ALTER TABLE `r_alur_status_map` DISABLE KEYS */;
INSERT INTO `r_alur_status_map` (`id`, `idAlurStatus`, `kdAlurUnit`, `kdTujuan`, `opsi`, `idAlurStatusNext`, `sequence`, `ket`) VALUES
	(1, 1, '+03+02+01+', '+01+02+03+04+', 'Ajukan', 2, 1, NULL),
	(2, 2, '+03+', '+01+02+03+04+', 'Setuju & Teruskan', 4, 1, NULL),
	(3, 2, '+03+02+01+', '+01+02+03+04+', 'Tolak', 3, 2, NULL),
	(4, 4, '+03+', '+01+', 'Setuju', 6, 1, NULL),
	(5, 4, '+03+', '+01+02+03+04+', 'Tolak', 5, 2, NULL),
	(6, 20, '+03+', '+01+02+03+04+', 'Ajukan', 21, 1, NULL),
	(7, 21, '+03+', '+01+', 'Setuju', 23, 1, NULL),
	(8, 21, '+03+', '+01+02+', 'Tolak', 22, 2, NULL),
	(9, 4, '+03+', '+02+03+04+', 'Setuju & Teruskan', 7, 1, NULL),
	(10, 7, '+03+02+', '+02+03+04+', 'Setuju & Teruskan', 9, 1, NULL),
	(11, 7, '+03+02+', '+02+03+04+', 'Tolak', 8, 2, NULL),
	(12, 9, '+03+02+', '+01+02+', 'Setuju', 11, 1, NULL),
	(13, 9, '+03+02+', '+01+02+03+04+', 'Tolak', 10, 2, NULL),
	(14, 21, '+03+', '+02+04+', 'Setuju & Teruskan', 24, 1, NULL),
	(15, 24, '+03+02+', '+02+03+04+', 'Setuju & Teruskan', 26, 1, NULL),
	(16, 24, '+03+02+', '+02+03+04+', 'Tolak', 25, 2, NULL),
	(17, 26, '+03+02+', '+01+02+', 'Setuju', 28, 1, NULL),
	(18, 26, '+03+02+', '+01+02+03+04+', 'Tolak', 27, 2, NULL),
	(19, 37, '+03+02+', '+02+03+04+', 'Ajukan', 39, 1, NULL),
	(20, 39, '+03+02+', '+02+03+04+', 'Setuju & Teruskan', 41, 1, NULL),
	(21, 39, '+03+02+', '+02+03+04+', 'Tolak', 40, 2, NULL),
	(22, 41, '+03+02+', '+01+02+', 'Setuju', 43, 1, NULL),
	(23, 41, '+03+02+', '+01+02+03+04+', 'Tolak', 42, 2, NULL),
	(24, 9, '+03+02+', '+03+04+', 'Setuju & Teruskan', 12, 1, NULL),
	(25, 12, '+03+02+', '+03+04+', 'Setuju & Teruskan', 14, 1, NULL),
	(26, 12, '+03+02+', '+03+04+', 'Tolak', 13, 2, NULL),
	(27, 14, '+03+02+', '+03+', 'Setuju', 16, 1, NULL),
	(28, 14, '+03+02+', '+03+04+', 'Tolak', 15, 2, NULL),
	(29, 26, '+03+02+', '+03+04+', 'Setuju & Teruskan', 29, 1, NULL),
	(30, 29, '+03+02+', '+03+04+', 'Setuju & Teruskan', 31, 1, NULL),
	(31, 29, '+03+02+', '+03+04+', 'Tolak', 30, 2, NULL),
	(32, 31, '+03+02+', '+03+', 'Setuju', 33, 1, NULL),
	(33, 31, '+03+02+', '+03+04+', 'Tolak', 32, 2, NULL),
	(34, 41, '+03+02+', '+03+04+', 'Setuju & Teruskan', 44, 1, NULL),
	(35, 44, '+03+02+', '+03+04+', 'Setuju & Teruskan', 46, 1, NULL),
	(36, 44, '+03+02+', '+03+04+', 'Tolak', 45, 2, NULL),
	(37, 46, '+03+02+', '+03+', 'Setuju', 48, 1, NULL),
	(38, 46, '+03+02+', '+03+04+', 'Tolak', 47, 2, NULL),
	(39, 14, '+03+02+', '+04+', 'Setuju & Teruskan', 17, 1, NULL),
	(40, 17, '+03+02+', '+04+', 'Setuju', 19, 1, NULL),
	(41, 17, '+03+02+', '+04+', 'Tolak', 18, 2, NULL),
	(42, 31, '+03+02+', '+04+', 'Setuju & Teruskan', 34, 1, NULL),
	(43, 34, '+03+02+', '+04+', 'Setuju', 36, 1, NULL),
	(44, 34, '+03+02+', '+04+', 'Tolak', 35, 2, NULL),
	(45, 46, '+03+02+', '+04+', 'Setuju & Teruskan', 49, 1, NULL),
	(46, 49, '+03+02+', '+04+', 'Setuju', 51, 1, NULL),
	(47, 49, '+03+02+', '+04+', 'Tolak', 50, 2, NULL),
	(48, 2, '+02+01+', '+01+02+03+04+', 'Setuju & Teruskan', 52, 1, NULL),
	(49, 52, '+02+', '+01+', 'Setuju & Teruskan', 9, 1, NULL),
	(50, 52, '+02+', '+01+02+03+04+', 'Tolak', 53, 2, NULL),
	(51, 20, '+02+', '+01+02+03+04+', 'Ajukan', 55, 1, NULL),
	(52, 55, '+02+', '+01+', 'Setuju & Teruskan', 26, 1, NULL),
	(53, 55, '+02+', '+01+02+03+04+', 'Tolak', 56, 2, NULL),
	(56, 37, '+02+', '+01+', 'Ajukan', 41, 1, NULL),
	(57, 52, '+02+', '+02+03+04+', 'Setuju & Teruskan', 7, 1, NULL),
	(59, 55, '+02+', '+02+03+04+', 'Setuju & Teruskan', 24, 1, NULL),
	(60, 57, '+02+', '+03+04+', 'Ajukan', 59, 1, NULL),
	(61, 59, '+02+', '+03+04+', 'Setuju & Teruskan', 61, 1, NULL),
	(62, 59, '+02+', '+03+', 'Tolak', 60, 2, NULL),
	(63, 61, '+02+', '+03+', 'Setuju', 63, 1, NULL),
	(64, 61, '+02+', '+03+04+', 'Tolak', 62, 2, NULL),
	(65, 61, '+02+', '+04+', 'Setuju & Teruskan', 64, 1, NULL),
	(66, 64, '+02+', '+04+', 'Setuju', 66, 1, NULL),
	(67, 64, '+02+', '+04+', 'Tolak', 65, 2, NULL);
/*!40000 ALTER TABLE `r_alur_status_map` ENABLE KEYS */;

-- Dumping structure for table pbn_izip.r_alur_tujuan
CREATE TABLE IF NOT EXISTS `r_alur_tujuan` (
  `kdTujuan` char(2) NOT NULL,
  `nmTujuan` varchar(50) NOT NULL,
  PRIMARY KEY (`kdTujuan`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Dumping data for table pbn_izip.r_alur_tujuan: 4 rows
DELETE FROM `r_alur_tujuan`;
/*!40000 ALTER TABLE `r_alur_tujuan` DISABLE KEYS */;
INSERT INTO `r_alur_tujuan` (`kdTujuan`, `nmTujuan`) VALUES
	('01', 'Dalam Kota Dalam Provinsi'),
	('02', 'Luar Kota Dalam Provinsi'),
	('03', 'Luar Provinsi Dalam Negeri'),
	('04', 'Luar Negeri');
/*!40000 ALTER TABLE `r_alur_tujuan` ENABLE KEYS */;

-- Dumping structure for table pbn_izip.r_alur_unit
CREATE TABLE IF NOT EXISTS `r_alur_unit` (
  `kdAlurUnit` char(2) NOT NULL,
  `nmAlurUnit` varchar(255) NOT NULL,
  `singkatan` varchar(50) NOT NULL,
  PRIMARY KEY (`kdAlurUnit`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table pbn_izip.r_alur_unit: 3 rows
DELETE FROM `r_alur_unit`;
/*!40000 ALTER TABLE `r_alur_unit` DISABLE KEYS */;
INSERT INTO `r_alur_unit` (`kdAlurUnit`, `nmAlurUnit`, `singkatan`) VALUES
	('01', 'Kantor Pusat', 'Kanpus'),
	('02', 'Kantor Wilayah', 'Kanwil'),
	('03', 'Kantor Pelayanan Perbendaharaan Negara', 'KPPN');
/*!40000 ALTER TABLE `r_alur_unit` ENABLE KEYS */;

-- Dumping structure for table pbn_izip.user_menus
CREATE TABLE IF NOT EXISTS `user_menus` (
  `id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `link_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `category` tinyint(1) unsigned NOT NULL,
  `have_child` tinyint(1) unsigned NOT NULL DEFAULT 0,
  `parent_id` smallint(3) unsigned NOT NULL,
  `sequence` tinyint(2) NOT NULL DEFAULT 1,
  `active` tinyint(1) unsigned NOT NULL DEFAULT 1,
  `granted_role` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '+00+',
  `icon` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table pbn_izip.user_menus: ~14 rows (approximately)
DELETE FROM `user_menus`;
/*!40000 ALTER TABLE `user_menus` DISABLE KEYS */;
INSERT INTO `user_menus` (`id`, `title`, `link_url`, `category`, `have_child`, `parent_id`, `sequence`, `active`, `granted_role`, `icon`, `filename`, `created_at`, `updated_at`) VALUES
	(1, 'Beranda', 'home', 1, 0, 0, 1, 0, '+00+', 'glyphichon glyphicon-dashboard', NULL, '2019-02-12 14:10:24', '0000-00-00 00:00:00'),
	(2, 'Dokumen', '#', 1, 1, 0, 2, 1, '+00+', 'glyphicon glyphicon-folder-open', NULL, '2019-02-12 14:49:28', '0000-00-00 00:00:00'),
	(3, 'Surat Masuk', '#', 2, 1, 2, 1, 1, '+00+', 'glyphicon glyphicon-record', NULL, '2019-02-12 14:37:03', '0000-00-00 00:00:00'),
	(4, 'Semua Surat', 'surat-masuk', 3, 0, 3, 1, 1, '+00+', 'glyphicon glyphicon-asterisk', NULL, '2019-02-14 14:54:01', '0000-00-00 00:00:00'),
	(5, 'Non Undangan', '#', 3, 0, 3, 2, 1, '+00+', 'glyphicon glyphicon-asterisk', NULL, '2019-02-15 09:38:12', '0000-00-00 00:00:00'),
	(6, 'Undangan', '#', 3, 0, 3, 3, 1, '+00+', 'glyphicon glyphicon-asterisk', NULL, '2019-02-15 09:38:08', '0000-00-00 00:00:00'),
	(7, 'Agenda Rapat', '#', 3, 0, 3, 4, 1, '+00+', 'glyphicon glyphicon-asterisk', NULL, '2019-03-29 20:15:57', '0000-00-00 00:00:00'),
	(8, 'Referensi', '#', 1, 1, 0, 99, 1, '+00+', 'glyphicon glyphicon-book', NULL, '2019-03-29 20:15:27', '0000-00-00 00:00:00'),
	(9, 'User', 'ref/user', 2, 0, 8, 1, 1, '+00+', 'glyphicon glyphicon-record', NULL, '2019-02-15 09:16:43', '0000-00-00 00:00:00'),
	(10, 'Unit', 'ref/unit', 2, 0, 8, 2, 1, '+00+', 'glyphicon glyphicon-record', NULL, '2019-02-15 09:16:46', '0000-00-00 00:00:00'),
	(11, 'Sekretaris', 'ref/sekre', 2, 0, 8, 3, 1, '+00+', 'glyphicon glyphicon-record', NULL, '2019-02-15 09:16:49', '0000-00-00 00:00:00'),
	(12, 'Plt./Plh.', 'ref/plt', 2, 0, 8, 4, 1, '+00+', 'glyphicon glyphicon-record', NULL, '2019-02-15 09:16:53', '0000-00-00 00:00:00'),
	(13, 'Izin Prinsip', '#', 1, 1, 0, 3, 1, '+00+', '', '', '2019-07-01 09:15:37', '0000-00-00 00:00:00'),
	(14, 'Input', 'izin-prinsip/input', 2, 0, 13, 1, 1, '+00+', '', 'izinprinsip-input.html', '2019-07-01 09:16:13', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `user_menus` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
